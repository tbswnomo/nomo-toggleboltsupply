<?php
include_once("NoMo/nomo_SDK_lib.php");
include_once("productDataParser.php");

    session_start();
    $status = $_GET[NOMO_FIELD_STATUS];
    $dest = "index.php?status=$status";
    $inCheckout = NOMO_SESSION_IS_CHECKOUT_IN_PROGRESS();
    if ($inCheckout){
        $dest = "productCart.php?status=$status";
    }

    header("Location:".$dest);
?>
