<?php
include_once("NoMo/nomo_SDK_lib.php");
ini_set('display_errors', 'On');
session_start();

//NoMo Integration Begin

//Include the NoMo SDK. This is done by including the 
//one file that contains all the includes for the NoMo SDK.

//NoMo Integration End

function fetchNoMoCredentials(){
    try{

        //NoMo Integration Begin
            //When the NoMo SDK receives a user introduction response the NoMo SDK calls this page.
            //At this point we need get the transaction id and the NoMo Issuer server URL that we will interact with.
            //The NoMo SDK has placed this data on the Query string of this page, so fetch it from there:
            $nomoServer = $_GET[NOMO_FIELD_NOMO_ISSUER];
            if (!isset($nomoServer))
                return "missing NoMo Server";
            $nomoTransactionId = $_GET[NOMO_FIELD_TRANSACTION_ID];
            if(!isset($nomoTransactionId))
                return "missing NoMo Transaction Id";;

            //Next we will need to call a NoMo SDK function that will fetch the Introduction Data
            //(I.E the unique user deviceId and the User Name alias.
            //The DeviceID is a uniquely generated number identifying this user.
            //The alias is a name chosen by the user for use when displaying the user's Name.
            $nomo = new NOMO_SDK();
            $logonData = $nomo->NOMO_FETCH_INTRODUCTION(
                $nomoServer,          // NoMo Issuer Server URL fetched from query string
                $nomoTransactionId);  // NoMo Transaction Id for this transaction also form the query string.

            //When calling the NoMo SDK always check the SDK status field for success.
            //See the NoMo SDK documentation for other status codes that may be returned by this call.
            if ($nomo->status != NOMO_STATUS_SUCCESS){
                return "NoMo Fetch Introduction failed ($nomo->status) data: ".json_encode($logonData);
            }

            //We will use the DeviceID in later calls to NoMo and the UserAlias for
            //display on the site to make the user feel more welcome.
            //So Store them for later use.
            $nomoDeviceId = $nomo->deviceId;
            $nomoAlias = $nomo->userAlias;

            //Acknowledge the receipt of the introduction. This lets NoMo Know the
            //transaction is active for future API calls
            $data = $nomo->NOMO_ACCEPT_INTRODUCTION($nomoServer, $nomoTransactionId);
            if ($nomo->status != NOMO_STATUS_SUCCESS){
                return "NoMo Accept Introduction failed ($nomo->status) data: ".json_encode($data);
            }

        //NoMo Integration End

        //For this demo valid introductions are any deviceId.
        //Otherwise validate the deviceId for qualifying permissions to your site.

        // For this demo we'll store the user and NoMo data in the session and cookie for use
        // in other site pages.

        session_start();

        $_SESSION[NOMO_FIELD_NOMO_ISSUER]=$nomoServer;
        $_SESSION[NOMO_FIELD_TRANSACTION_ID]=$nomoTransactionId;
        $_SESSION[NOMO_FIELD_DEVICE_ALIAS]=$nomoAlias;
        $_SESSION[NOMO_FIELD_DEVICE_ID]=$nomoDeviceId;

        //We will store the Device introduction in a cookie so that if the user
        //returns at a later date they can take advantage of the NoMo fast
        //checkout feature. Fast checkout allows the user to skip the scanning of
        //the QR code when we have their device Id on hand.
        NOMO_COOKIE_STORE(NOMO_FIELD_DEVICE_ID, $nomoDeviceId);
        NOMO_COOKIE_STORE(NOMO_FIELD_DEVICE_ALIAS, $nomoAlias);
    }catch(Exception $ex){
        return "Exception: ". $ex;
    }
    return null;
}

$results = fetchNoMoCredentials();
if (!isset($results)){
    //Route the user to the next page in this workflow.
    $Checkout = NOMO_SESSION_IS_CHECKOUT_IN_PROGRESS();
    if ($Checkout){
        header("Location:productCheckout.php");
        exit;
    }

};

// errors encountered cancel a checkout if there is one active and route to home page
NOMO_SESSION_CHECKOUT_CANCEL();

session_write_close();

Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,"loginprocess","Result Error".$results);

header("Location:index.php");

?>
