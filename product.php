<?php
include_once("productDataParser.php");
session_start();

$productID = $_GET["SKU"];
$prod = readProduct($productID);

$productName = $prod->name;
$productDescription = $prod->description;
$productMainImage = $prod->getImage();
$basePrice =  "$" . number_format($prod->basePrice, 2, ".", ",");

function addProductAsInquiryItem($prod){
    $output = "";
    if (count($prod->options )< 1){
        return $output;
    }

    $output .=  "Options:"."\n";
    $output .=  "<UL style='list-style: none;'>"."\n";
    foreach($prod->options as $prodOption){
        $optionPrice = "$" . number_format($prodOption->price, 2, ".", ",");
        $output .=  "<li>$prodOption->name ($optionPrice)</li>"."\n";
    }
    $output .=  "</UL>"."\n";
    $output .= "<br>";
    $output .= '<a href="productInquiry.php?SKU='.$prod->sku.'">';
    $output .= '<img src="images'.DIRECTORY_SEPARATOR.'productInquiry1.jpg';
    $output .= 'border="0"';
    $output .= 'alt="product inquiry">';
    $output .= '</a>'."\n";

    return $output;
}

function addProductAsCartItem($prod){

    $output = '<form method="post" action="productCart.php?SKU='.$prod->sku.'" name="Product Cart">';
    $output .= '<table>'."\n";

    $options = $prod->options;
    if (count ($options) > 0){
        $optNumber = 0;
        foreach ($options as $option){
            $optionPrice = "$" . number_format($option->price, 2, ".", ",");
            $optionVal = $option->name . "(" . $optionPrice . ")";
            $optNumber = $optNumber + 1;
            $output .= '<tr><td>';
            $output .= '<input type="checkbox"';
            $output .= ' name="option'.$optNumber.'"';
            $output .= ' value="'.$optNumber.'">';
            $output .= '</td>';
            $output .= '<td>';
            $output .= $optionVal;
            $output .= '</td></tr>'."\n";
        }
    }
    $output .= '<tr><td colspan="2" align="center">';
    $output .= '<INPUT class = "Button" type="submit" name="AddToCart" value="Add To Cart">';
    $output .= '</td></tr>'."\n";
    $output .= '</table>'."\n";

    return $output;
}


?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <?php include("head.php"); ?>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser.
                <a href="http://browsehappy.com/">Upgrade your browser today</a> or
                <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a>
                to better experience this site.</p>
        <![endif]-->

        <div id="wrapper">
            <header>
                <?php include("header.php"); ?>
            </header>

            <section>
                <div id="container">
                    <div id="content">
                        <article>
                            <?php
                                echo "<div style='text-align: center;'>"."\n";
                                echo "<h3>$productName</h3>"."\n";
                                echo "<table width='100%'>"."\n";
                                    echo "<tr>"."\n";
                                    echo "<td>"."\n";
                                        echo "<a href='productImageViewer.php?SKU=$productID'>
                                               <img src='products".DIRECTORY_SEPARATOR."$productMainImage'
                                               alt='$productName'
                                               height='200'
                                               width='200'></a>"."\n";
                                        echo "<h5>$prod->sku</h4>"."\n";
                                        echo "<sub><i>Click on image for larger.</i></sub>"."\n";
                                    echo "</td>"."\n";
                                    echo "<td valign='top'>"."\n";
                                        echo "<table>"."\n";
                                            echo "<tr  align='left'>"."\n";
                                            echo "<td>"."\n";
                                                if ($prod->basePrice == 0){
                                                    echo "Coming Soon!"."\n";
                                                } else {
                                                    $options = $prod->options;
                                                    if (count ($options) > 0){
                                                        echo "<b>Price starting at: ".$basePrice."</b>\n";
                                                    }else{
                                                        echo "<b>Price : $basePrice"."<b>\n";
                                                    }
                                                }
                                            echo "</td>"."\n";
                                            echo "</tr>"."\n";
                                            If (!$prod->isWebAvailable){
                                                echo "<tr  align='left'>"."\n";
                                                echo "<td>"."\n";
                                                    echo (addProductAsInquiryItem($prod));
                                                echo "</td>"."\n";
                                                echo "</tr>"."\n";
                                           }else{
                                                echo "<tr  align='left'>"."\n";
                                                echo "<td>"."\n";
                                                echo (addProductAsCartItem($prod));
                                                echo "</td>"."\n";
                                                echo "</tr>"."\n";
                                            }

                                        echo "</table>"."\n";
                                    echo "</td>"."\n";
                                    echo "</tr>"."\n";
                                echo "</table>"."\n";
                                echo "</div>"."\n";

                                echo "<br>"."\n";
                                echo "<p> $productDescription </p>"."\n";

                            echo "<br>";

                            echo "<img src='images".DIRECTORY_SEPARATOR."contentbg.gif' width='100%'>"."\n";

                            $relatedProducts = array();
                            foreach($prod->relatedSkus as $sku){
                                $relatedProd = readProduct($sku);
                                if ($relatedProd->isActive){
                                    $relatedProducts[] = $relatedProd;
                                }
                            }
                            if (count($relatedProducts) > 0){
                                echo "<h4> Related Products</h4>"."\n";
                                echo "<table width='100%'>"."\n";
                                echo "<tr align='center'>"."\n";

                                if(count($relatedProducts)&1) {
                                   $limit = 3;
                                } else {
                                    $limit = 2;
                                }
                                $count = 0;
                                foreach($relatedProducts as $relatedProd){
                                    $count = $count + 1;
                                    if ($count > $limit){
                                        echo "</tr><tr>"."\n";
                                        $count = 0;
                                    }

                                    echo "<td align='center'>"."\n";
                                    echo "<h5> $relatedProd->name</h5>"."\n";
                                    $link =  "<a href='product.php?SKU=$relatedProd->sku'>"."\n";
                                    $link .= "<img src='products".DIRECTORY_SEPARATOR."$relatedProd->image'";
                                    $link .= "   alt='$relatedProd->name'";
                                    $link .= "   height='75'";
                                    $link .= "   width='75'></a>"."\n";
                                    echo $link;
                                    echo "</td>"."\n";
                                }
                                echo "</tr>"."\n";
                                echo "</table>"."\n";
                            }
                            $numLinks = count($prod->links);
                            if ($numLinks > 0){
                                echo "<h4>Useful Sites</h4>"."\n";
                                echo "<ul>"."\n";
                                foreach($prod->links as $link){
                                    echo "<li><a href='$link->url'>$link->name</a>"."\n";
                                }
                                echo "</ul>"."\n";
                            }
                            ?>
                        </article>
                    </div><!-- #content-->
                </div><!-- #container-->

                <?php include("menuSide.php"); ?>
            </section>

            <footer>
                <?php include("copyright.php"); ?>
            </footer>

        </div><!-- #wrapper -->

    </body>
</html>
