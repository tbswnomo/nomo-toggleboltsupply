<?php
    include_once "productDataParser.php";
?>

<aside id='left'>

    <?php generateProductMenu("", ""); ?>

    <div class="section">
        <h3>Links</h3>
        <div class='inner'>
            <ul>
                <li>
                    <a href="http://www.toggleboltsw.com"
                       target="_blank">
                        <img alt="Togglebolt Software Inc."
                             src="images/ToggleboltSWLogo.png" />
                    </a>
                </li>
                <li>
                    <a href="https://play.google.com/store/apps/details?id=com.toggleboltsw.nomo"
                        target="_blank">
                        <img alt="Android app on Google Play"
                             src="images/gogglePlayStore-small.png" />
                    </a>
                </li>
            </ul>
        </div>
    </div><!-- .section -->
</aside><!-- #left -->

