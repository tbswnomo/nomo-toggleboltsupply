<?php
include_once("productDataParser.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <?php include("head.php"); ?>
</head>

<body>

<div id="wrapper">
	<header>
		<div id="description">Toggle Bolts for every need.</div>
        <?php include("menuTop.php"); ?>
    </header>

	<section>
        <div id="container">
            <div id="content">
                <article>
                    <h2><a href="#" rel="bookmark">Contact</a></h2>
                    <div class="entry">
                        <p>
                            Thank your for your comments, If requested we will respond with in 48 Hours on most occasions.
                        </p>
                    </div>
                </article>
            </div><!-- #content-->
		</div><!-- #container-->

        <?php include("menuSide.php"); ?>

        <footer>
            <?php include("copyright.php"); ?>
	</footer>

</div><!-- #wrapper -->
</body>
</html>
