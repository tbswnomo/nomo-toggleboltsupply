<?php
    include_once("productDataParser.php");
    ini_set('display_errors', 'On');
    session_start();

    unset($_COOKIE[NOMO_FIELD_DEVICE_ID]);
    unset($_COOKIE[NOMO_FIELD_DEVICE_ALIAS]);

    $_SESSION[LoggedIn]=false;
    $_SESSION[NOMO_FIELD_DEVICE_ID]=null;
    $_SESSION[NOMO_FIELD_NOMO_ISSUER]=null;
    $_SESSION[NOMO_FIELD_TRANSACTION_ID]=null;

    header("Location:index.php?");

