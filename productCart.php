<?php
ini_set('display_errors', 'On');
include_once("NoMo/nomo_SDK_lib.php");
include_once("productDataParser.php");

session_start();
NOMO_SESSION_CHECKOUT_CANCEL();
$products = $_SESSION['ShoppingCart'];
if (!isset($products))
    $products = array();

$status = null;
if (!empty($_GET[NOMO_FIELD_STATUS])) {
    $status = $_GET[NOMO_FIELD_STATUS];
}

if (isset($_GET["RESET"])){
    NOMO_SESSION_CLEAR(false);
    NOMO_COOKIE_CLEAR();
}

$sku = "";
$action = "";
if (isset($_GET["SKU"])){
    $action = "add";
    $sku = $_GET["SKU"];
    $prod = readProduct($sku);
    if (isset($prod) && !empty($prod->sku)){
        if (!array_key_exists($sku,$products)){
            $products[$sku] = 1;
        }else{
            $products[$sku] += 1;
        }
    }
}
if (isset($_GET["DELETE"])){
    $action = "delete";
    $sku = $_GET["DELETE"];
    if (array_key_exists($sku,$products)){
        unset($products[$sku]);
    }
}
$_SESSION[ShoppingCart] = $products;
session_write_close();

?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <?php include("head.php"); ?>
    </head>
    <body>
<!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser.
                <a href="http://browsehappy.com/">Upgrade your browser today</a> or
                <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a>
                to better experience this site.</p>
        <![endif]-->

        <div id="wrapper">
            <header>
                <?php include("header.php"); ?>
            </header>

            <section>
                <div id="container">
                    <div id="content">
                        <article>
                            <?php
                                $output = getCartDisplay($action,$products, $sku);
                                echo ($output);
                            ?>
                        </article>
                        <article>
                            <?php
                                $output = getRetryNote($status);
                                echo ($output);
                            ?>
                        </article>
                        <article>
                            <br><br>
                            <p>Click this start a fresh demo, Clears the current user cookie</p>
                            <form method="post" action="productCart.php?RESET=true">

                                <input type="submit"
                                       class="Button Delete"
                                       value="Reset the Session Cookie">
                            </form>
                        </article>
                    </div><!-- #content-->
                </div><!-- #container-->

                <?php include("menuSide.php"); ?>
            </section>

            <footer>
                <?php include("copyright.php"); ?>
            </footer>

        </div><!-- #wrapper -->

    </body>
</html>

<?php
function getCartItemDisplay($productSku, $qnt){
    $item = readProduct($productSku);
    $cost = floatval($item->basePrice);
    $itemPrice = "$" . number_format($cost, 2, ".", ",");
    $itemQnt = $qnt;
    $itemDesc = $item->name;
    $itemCost = "$" . number_format(($cost * $qnt), 2, ".", ",");

    $output = '<tr>';
    $output .= '<td>'.$itemDesc.'</td>';
    $output .= '<td>'.$itemPrice.'</td>';
    $output .= '<td>'.$itemQnt.'</td>';
    $output .= '<td>'.$itemCost.'</td>';
    $output .= '</tr>'."\n";
    echo $output;
    return $cost * $qnt;
}

function getCartItemsTable($products){
    $output = "";
    $output .='<table>'."\n";
    $output .='    <tr>';
    $output .='        <th>Description</th>';
    $output .='        <th>&nbsp;</th>';
    $output .='        <th >Price</th>';
    $output .='        <th>&nbsp;</th>';
    $output .='        <th>Quantity</th>';
    $output .='        <th>&nbsp;</th>';
    $output .='        <th>Cost</th>';
    $output .='    </tr>'."\n";

    $subTotal = 0.0;
    foreach ($products as $sku => $quantity){
        $item = readProduct($sku);
        $cost = floatval($item->basePrice);
        $itemPrice = "$" . number_format($cost, 2, ".", ",");
        $itemQnt = $quantity;
        $itemDesc = $item->name;
        $itemCost = "$" . number_format(($cost * $quantity), 2, ".", ",");
        $subTotal += $quantity * $cost;

        $output .= '    <tr>';
        $output .= '<td align="left">'.$itemDesc.'</td>';
        $output .= '<td>&nbsp;</td>';
        $output .= '<td align="right">'.$itemPrice.'</td>';
        $output .= '<td>&nbsp;</td>';
        $output .= '<td align="right">'.$itemQnt.'</td>';
        $output .= '<td>&nbsp;</td>';
        $output .= '<td align="right">'.$itemCost.'</td>';
        $output .= '<td>&nbsp;</td>';
        $output .= '<td align="right">';
        $output .= '<form method="post" action="productCart.php?DELETE='.$sku.'" name="Delete">';
        $output .= '<INPUT class="Button Delete" type="submit" name="delete" value="Delete"></form>';
        $output .= '</td>';
        $output .= '</tr>'."\n";
    }
    $output .= '</table>'."\n";

    $fees = ($subTotal * 0.03);
    $tax = ($subTotal * 0.0625);
    $shipping = ($subTotal * 0.04);
    $total = $subTotal + $fees + $shipping + $tax;
    $displaySubTotal = "$" . number_format($subTotal, 2, ".", ",");
    $displayTax = "$" . number_format($tax, 2, ".", ",");
    $displayFees = "$" . number_format($fees, 2, ".", ",");
    $displayShipping = "$" . number_format($shipping, 2, ".", ",");
    $displayTotal = "$" . number_format($total, 2, ".", ",");

    $output .= '<table>'."\n";
    $output .= '    <tr><td>&nbsp;</td></tr>'."\n";
    $output .= '    <tr><td>Sub Total</td><td>&nbsp;</td><td align="right">'.$displaySubTotal.'</td></tr>'."\n";
    $output .= '    <tr><td>Packaging and processing Fees</td><td>&nbsp;</td><td align="right">'.$displayFees.'</td></tr>'."\n";
    $output .= '    <tr><td>Tax</td><td>&nbsp;</td><td align="right">'.$displayTax.'</td></tr>'."\n";
    $output .= '    <tr><td>Shipping</td><td>&nbsp;</td><td align="right">'.$displayShipping.'</td></tr>'."\n";
    $output .= '    <tr><td><b>Total<b></td><td>&nbsp;</td><td align="right"><b>'.$displayTotal.'</b></td></tr>'."\n";
    $output .= '</table>'."\n";

    return $output;
}

function getCartDisplay($action, $products, $sku){
    $output = "";
    $prod = readProduct($sku);
    if (isset($prod) && !empty($prod->sku)){
        if ($action == "add"){
            $output .= "<h2>".$prod->getName()." has been added to your cart."."</h2>"."\n";
        }else if ($action == "delete"){
            $output .= "<h2>".$prod->getName()." has been removed from your cart."."</h2>"."\n";
        }
        $output .= '<br>';
    }

    $dest = "";
    if (isset($sku) && !empty($sku)){
        $dest = "product.php?SKU=$sku";
    }else{
        $dest = "index.php";
    }

    if (count($products) < 1){
        $output .= '<h3>Your Cart is Empty</h3>'."\n";
        $output .= '<form method="post" action="'.$dest.'" name="product">';
        $output .= getResumeShoppingButton();
        $output .= '</form>'."\n";
        return $output;
    }

    $output .= getCartItemsTable($products);

    $output .= '<table>'."\n";
    $output .= '    <tr>'."\n";
    $output .= '        <td>'."\n";
    $output .= '            <div class="CheckoutButton">';
    $output .= '<button class="Button" onClick="window.location=\'login.php?'.NOMO_FIELD_TRANSACTION_STATE.'='.NOMO_FIELD_TRANSACTION_STATE_STARTED.'\'">';
    $output .= '<img src="images\NoMo-Logo.png" height="33" width="25">&nbsp NoMo Checkout</button>';
    $output .= '            </div>'."\n";
    $output .= '        </td>'."\n";
    $output .= '        <td>&nbsp;</td>'."\n";
    $output .= '        <td>'."\n";
    $output .= getResumeShoppingButton();
    $output .= '        </td>'."\n";
    $output .= '    </tr>'."\n";
    $output .= '</table>'."\n";

    return $output;
}

function getResumeShoppingButton(){
    $output = '            <div class="ResumeShoppingButton">';
    $output .= '                <button class="Button Search" onClick="window.location=\''.$dest.'\'"> Resume<br>Shopping </button>';
    $output .= '            </div>'."\n";
    return $output;
}

function getRetryNote($status){
    $out = "";
    if (!isset($status)){
        return $out;
    }

    $out .= "<br><h4 style='color:red'>";
    $out .= "Please retry the checkout.";
    $out .= "</h4>";

    $out .= "<h5 style='color:red'>";
    $out .= "The previous checkout attempt has encountered an error:";
    $out .= "<br>";
    $out .= "(".$status.") - ".NomoStatusDescriptions::getDescription($status);
    $out .= "</h5>";
    return $out;
}

?>
