<?php
ini_set('display_errors', 'On');
include_once("NoMo/nomo_SDK_lib.php");
include_once("productDataParser.php");

session_start();
//If no products or our local cart is empty, then navigat back to the cart page.
$products = $_SESSION[ShoppingCart];
if (!isset($products) || count($products)< 1) {
    header("Location:index.php");
    exit;
}


$inCheckout = NOMO_SESSION_IS_CHECKOUT_IN_PROGRESS();
if (!$inCheckout){
    header("Location:index.php");
    exit;
}

?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <?php include("head.php"); ?>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser.
                <a href="http://browsehappy.com/">Upgrade your browser today</a> or
                <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a>
                to better experience this site.</p>
        <![endif]-->

        <div id="wrapper">
            <header>
                <?php include("header.php"); ?>
            </header>

            <section>
                <div id="container">
                    <div id="content">
                        <article>
                            <!-- this page displays the checkout confirmation page.-->
                            <h2>Processing Checkout</h2>

                            <h3>Processing transaction with NoMo.</h3>
                            <p>The NoMo App on your phone is being paged with a request for Payment.</p>
                            <p>If the NoMo App is open on the phone the payment request will automatically appear within a few seconds.
                            <p>If the NoMo App is closed then your phone will receive a notification within a few seconds.
                               Accepting the notification will start the NoMO App. Login to the App with your master password to
                               see the payment request.</p>
                            <p>Use the NoMo App on your phone to Approve or Decline this transaction.</p>
                            <?php
                                $red = "<script>setTimeout(\"location.href = 'productProcessCheckout.php';\",0);</script>";
                                echo $red;
                            ?>
                        </article>
                    </div><!-- #content-->
                </div><!-- #container-->

                <?php include("menuSide.php"); ?>
            </section>

            <footer>
                <?php include("copyright.php"); ?>
            </footer>

        </div><!-- #wrapper -->

    </body>
</html>


