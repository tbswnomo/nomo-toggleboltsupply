<?php
    include_once("NoMo/nomo_SDK_lib.php");
    include_once("productDataParser.php");

    session_start();
    $dest = 'index.php';
    $inCheckout = NOMO_SESSION_IS_CHECKOUT_IN_PROGRESS();
    if ($inCheckout){
        $dest = 'productCart.php?status='.NOMO_STATUS_TRANSACTION_EXPIRED;
    }

    header("Location:".$dest);
?>