<?php
include_once "NoMo/nomo_SDK_lib.php";

session_start();

//This page may be called either to perform a straight login or
//at the start of a checkout. When Checkout we get notified with a URL argument.
//We will stroe that in the Session for other pages to use.
$inCheckout = false;
if (isset($_GET[NOMO_FIELD_TRANSACTION_STATE])){
    NOMO_SESSION_CHECKOUT_START();
    $inCheckout = true;
}

//if we have a device id in a cookie indicating
//the user is returning to our site.
$name = NOMO_COOKIE_FETCH_ALIAS();
$haveDeviceId = NOMO_COOKIE_HAS_DEVICE_ID();

//If we do have a device ID then setup the session with thier data.
if ($haveDeviceId){
    //place it in the session so we can have it as if the user logged in
    $_SESSION[NOMO_FIELD_DEVICE_ID] = NOMO_COOKIE_FETCH_DEVICE_ID();
    $_SESSION[NOMO_FIELD_DEVICE_ALIAS] = $name;
}

$nomo = new NOMO_SDK();
$isNoMoInstalled = $nomo->isMobileBrowser();
$introductionCode = "";

//We arrive here if 1) we do not have a device id stored or 2) the device id came from a cookie so we need to
//have the user validate they are how we think they are.
session_write_close();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <?php include("head.php"); ?>
</head>

<body>
<div id="wrapper">
    <header>
        <?php include("header.php"); ?>
    </header>

    <section>
        <div id="container">
            <div id="content">
                <article>
                    <h2 style="width:80%;margin-right: auto;margin-left: auto">Complete your Togglebolt Supply Order</h2>
                    <table>
                        <tr>
                            <td align="center">
                                <div style="width:80%;margin: auto;">
                                    <b><?php echo $introductionCode ?></b>
                                    <br>
                                </div>
                                <?php
                                session_start();
                                // NoMo Integration Begin

                                // This SDK call will echo a DIV block containing the NoMo QR code and
                                // associated Java Script that is needed to process the browser side login.
                                // You can customize the look of this thur CSS each element has a unique CSS id
                                // See the SDK documentation for detials.
                                // The Login process can have three resulting events:
                                // 1) A successful response for the user
                                // 2) A timeout, meaning we did not receive a response for the user in time.
                                // 3) A communication error when sending to the NoMo server.
                                // You pass references to your three PHP pages that the SDK will call for
                                // these events You may handel the events in any way you deem appropriate for your site.
                                $nomo = new NOMO_SDK();
                                $nomo->NOMO_REQUEST_INTRODUCTION(
                                            "loginprocess.php",
                                            "loginexpired.php",
                                            "loginerror.php",
                                            $inCheckout);
                                $introductionCode = $nomo->introductionCode;
                                // NoMo Integration End
                                ?>
                            </td>
                            <td align="top">
                                <h3>Start the NoMo checkout</h3>
                                <?php if ($isNoMoInstalled){ ?>
                                    <h4>Tap the NoMo App Icon to the left.</h4>
                                <?php }else{ ?>
                                    <h4>Returning customer:</h4>
                                    <?php if ($haveDeviceId && $inCheckout){ ?>
                                    <div class="fastCheckoutButton" style="margin-left: auto;margin-right: auto;">
                                        <form method="post" action="productCheckout.php?FASTCHECKOUT=true">
                                            <input type="submit"
                                                   style="width:80%;height:30;"
                                                   value="<?php echo $name; ?> Checkout">
                                        </form>
                                    </div>
                                    <br>
                                    <i>if you are not <?php echo $name ?> then</i>
                                    <br><br>
                                    <?php }?>
                                    <h5>Checkout by either:</h5>

                                    <ul style="list-style-type: none;">
                                        <li>
                                            Entering the Introduction code <b><?php echo $introductionCode ?></b> into the NoMo App
                                        </li>
                                        <li>
                                            Scanning the QR code with the NoMo app.
                                        </li>
                                    </ul>
                                <?php } ?>
                            </td>
                        </tr>
                    </table>
                </article>
            </div><!-- #content-->
        </div><!-- #container-->

        <?php include("menuSide.php"); ?>
    </section>

    <footer>
        <?php include("copyright.php"); ?>
    </footer>

</div><!-- #wrapper -->
</body>
</html>
