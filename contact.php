<?php
include_once("productDataParser.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <?php include("head.php"); ?>
</head>

<body>

<div id="wrapper">
	<header>
		<?php include("header.php"); ?>
        <?php include("menuTop.php"); ?>
    </header>

	<section>
        <div id="container">
            <div id="content">
                <article>
                    <h2><a href="#" rel="bookmark">Contact</a></h2>
                    <div class="entry">
                        <p>
                            <form method="post" action="formmail.php" name="ContactForm">
                                <input type="hidden" name="env_report"
                                       value="REMOTE_HOST,REMOTE_ADDR,HTTP_USER_AGENT,AUTH_TYPE,REMOTE_USER">
                                <input type="hidden" name="recipients" value="sales***toggleboltsw.com" />
                                <input type="hidden" name="required" value="email:Your email address,realname:Your name" />
                                <input type="hidden" name="subject" value="DF Inquiry" />
                                <input type="hidden" name="good_url" value="contactThankYou.php" />
                                <input type="hidden" name="bad_url"  value="formmailError.php" />

                            <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="90%">
                                <TR>
                                    <TD width="30%"><DIV align="right"><B>Name:</B></DIV></TD>
                                    <TD width="70%"><INPUT type="text" name="realname" size="20"></TD>
                                </TR>
                                <TR>
                                    <TD><DIV align="right"><B>Email:</B></DIV></TD>
                                    <TD><INPUT type="text" name="email" size="20"></TD>
                                </TR>
                                <TR>
                                    <TD><DIV align="right"><B>Comment:</B></DIV></TD>
                                    <TD><TEXTAREA name="comment" cols="30" wrap="virtual" rows="6"></TEXTAREA></TD>
                                </TR>
                                <TR>
                                    <TD>&nbsp;</TD>
                                    <TD>
                                        <INPUT type="submit" name="submit" value="Submit">
                                        <INPUT type="reset" name="reset" value="Reset">
                                    </TD></TR>
                            </TABLE>
                        </FORM>
                        </p>
                    </div>
                </article>
            </div><!-- #content-->
		</div><!-- #container-->

        <?php include("menuSide.php"); ?>
    </section>

        <footer>
            <?php include("copyright.php"); ?>
	</footer>

</div><!-- #wrapper -->
</body>
</html>
