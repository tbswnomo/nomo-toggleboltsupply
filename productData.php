<?php
include("productLink.php");
include("productOption.php");

class productData {
    public $isActive = false;
    public $isWebAvailable = false;
    public $category = "";
    public $sku = "";
    public $name = "";
    public $description = "";
    public $image = "";
    public $alternateImages = array();
    public $basePrice = 0;
    public $relatedSkus = array();
    public $links = array();
    public $options = array();


    public function setCategory($category)
    {
        $this->category = $category;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    public function getSku()
    {
        return $this->sku;
    }

    public function setBasePrice($price)
    {
        $this->basePrice = $price;
    }

    public function getBasePrice()
    {
        return $this->basePrice;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    public function getIsActive()
    {
        return $this->isActive;
    }

    public function setIsWebAvailable($isWebAvailable)
    {
        $this->isWebAvailable = $isWebAvailable;
    }

    public function getIsWebAvailable()
    {
        return $this->isWebAvailable;
    }

    public function setLinks($links)
    {
        $this->links = $links;
    }

    public function getLinks()
    {
        return $this->links;
    }

    public function setRelatedSkus($relatedSku)
    {
        $this->relatedSkus = $relatedSku;
    }

    public function getRelatedSkus()
    {
        return $this->relatedSkus;
    }

    public function setAlternateImages($alternateImages)
    {
        $this->alternateImages = $alternateImages;
    }

    public function getAlternateImages()
    {
        return $this->alternateImages;
    }
}
?>