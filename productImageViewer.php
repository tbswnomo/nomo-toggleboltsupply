<?php
include_once("productDataParser.php");

$productID = $_GET["SKU"];
$prod = readProduct($productID);
$productName = $prod->name;
$altImageList = $prod->alternateImages;
$mainImage = $prod->getImage();

$displayImage  = 0;
if (isset($_GET["display"])) {
    $displayImage = $_GET["display"];
}

?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <?php include("head.php"); ?>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser.
                <a href="http://browsehappy.com/">Upgrade your browser today</a> or
                <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a>
                to better experience this site.</p>
        <![endif]-->

        <div id="wrapper">
            <header>
                <?php include("header.php"); ?>
            </header>

            <section>
                <div id="container">
                    <div id="content">
                        <article>
                            <div style='text-align: left;'>
                                <?php
                                echo "<h3>$productName - $prod->sku</h3>\n";
                                $imageName = $mainImage;
                                if ($displayImage > 0){
                                    $imageName = $altImageList[$displayImage - 1];
                                }
                                echo "<a href='product.php?SKU=$prod->sku'>Back</a>\n";
                                echo "<table>\n";
                                echo "<tr>\n";
                                echo "<td><img src='products".DIRECTORY_SEPARATOR."$imageName'
                                           alt='$productName'
                                           width='450'>\n";
                                echo "</td>\n";
                                echo "<td valign='top'>\n";
                                echo "<table>\n";
                                $arrIdx = 0;
                                foreach($altImageList as $altImage){
                                    $arrIdx = $arrIdx + 1;
                                    echo "<tr>\n";
                                    echo "<td>\n";
                                    echo "<a href='productImageViewer.php?SKU=$productID&display=$arrIdx'>
                                            <img src='products".DIRECTORY_SEPARATOR."$altImage'
                                                 height='75'
                                                 width='75'></a>\n";
                                    echo "</td>\n";
                                    echo "</tr>\n";
                                }
                                echo "</table>\n";
                                echo "</td>\n";
                                echo "</tr>\n";
                                echo "</table>\n";
                                ?>
                            </div>
                        </article>
                    </div><!-- #content-->
                </div><!-- #container-->

                <?php include("menuSide.php"); ?>
            </section>

            <footer>
                <?php include("copyright.php"); ?>
            </footer>

        </div><!-- #wrapper -->

    </body>
</html>
