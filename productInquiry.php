<?php
include_once("productDataParser.php");
$productID = $_GET["SKU"];
$prod = readProduct($productID);

$productName = $prod->name;
$productDescription = $prod->description;
$productMainImage = $prod->getImage();
$options = $prod->options;
$basePrice =  "$" . number_format($prod->basePrice, 2, ".", ",");

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <?php include("head.php"); ?>
</head>

<body>

<?php
$productID = $_GET["SKU"];
?>

<div id="wrapper">
	<header>
		<div id="description">Hand built quality products.</div>
        <?php include("menuTop.php"); ?>
    </header>

	<section>
        <div id="container">
            <div id="content">
                <article>
                    <?php
                    echo "<div style='text-align: center;'>";
                        echo "<h4>$productName<br>$prod->sku</h4>";
                        echo "<img src='products".DIRECTORY_SEPARATOR."$productMainImage'
                                               alt='$productName'
                                               height='100'
                                               width='100'>";
                        echo "<br>Price: $basePrice";
                    echo "</div>";
                    ?>
                    <br>

                    <div class="entry">
                        <p>
                        <form method="post"
                              action="formmail.php"
                              name="ProductInquiryForm">
                            <input type="hidden" name="env_report"
                                   value="REMOTE_HOST,REMOTE_ADDR,HTTP_USER_AGENT,AUTH_TYPE,REMOTE_USER">
                            <input type="hidden" name="recipients" value="sales***toggleboltsw.com" />
                            <input type="hidden" name="required" value="email:Your email address,realname:Your name" />
                            <input type="hidden"
                                   name="subject"
                                   value="
                                    <?php
                                       echo "TB Supply Product Inquiry : $productID - $productName"
                                    ?>
                                    "/>
                            <input type="hidden" name="good_url" value="productInquiryThankYou.php" />
                            <input type="hidden" name="bad_url"  value="formmailError.php" />
                            <input type="hidden" name="Price"  value="$basePrice" />

                            <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="90%">
                            <TR>
                                <TD width="30%"><DIV align="right"><B>Name:</B></DIV></TD>
                                <TD width="70%"><INPUT type="text" name="realname" size="20"></TD>
                            </TR>
                            <TR>
                                <TD><DIV align="right"><B>Email:</B></DIV></TD>
                                <TD><INPUT type="text" name="email" size="20"></TD>
                            </TR>
                            <?php
                            if (count ($options) > 0){
                                echo "<tr>";
                                echo "<TD><DIV align='right'><B>Options:</B></DIV></TD>";
                                echo "<td>";
                                $optNumber = 0;
                                foreach ($options as $option){
                                    $optionPrice = "$" . number_format($option->price, 2, ".", ",");
                                    $optionVal = $option->name . "(" . $optionPrice . ")";
                                    $optNumber = $optNumber + 1;
                                    echo "<input type='checkbox' name='option$optNumber' value='$optionVal'>$optionVal<br>";
                                }
                                echo "</td></tr>";
                            }
                            ?>
                            <TR>
                                <TD><DIV align="right"><B>Inquiry:</B></DIV></TD>
                                <TD><TEXTAREA name="inquiry" cols="30" wrap="virtual" rows="6"></TEXTAREA></TD>
                            </TR>
                            <TR>
                                <TD>&nbsp;</TD>
                                <TD>
                                    <INPUT type="submit" name="submit" value="Submit">
                                    <INPUT type="reset" name="reset" value="Reset">
                                </TD></TR>
                            </TABLE>
                        </FORM>
                        </p>
                    </div>
                </article>
            </div><!-- #content-->
		</div><!-- #container-->

        <?php include("menuSide.php"); ?>

        <footer>
            <?php include("copyright.php"); ?>
	</footer>

</div><!-- #wrapper -->
</body>
</html>
