<?php
include("productData.php");

define ("LastProductSku", "LastSKU");
define ("ShoppingCart", "ShoppingCart");

function getProductDBPath(){
    $filename = "products/products.xml";
    $path = $filename;
    if (!file_exists($path)){
        $path = "../" . $path;
    }
    return $path;
}

function readDatabase($category)
{
    $prodArray = array();
    $xml = simplexml_load_file(getProductDBPath());
    foreach ($xml->product as $product) {
        $prodObj = mapXmlToProduct($product);
        $str = trim($category, "");
        if (!empty($str)){
            if (strcasecmp($prodObj->category, $category) != 0){
               continue;
            }
        }
        $prodArray[] = $prodObj;
    }
    return $prodArray;
}

function readProduct($sku){
    $prodObj = new ProductData();
    $xml = simplexml_load_file(getProductDBPath());

    /* For each <character> node, we echo a separate <name>. */
    foreach ($xml->product as $productXML) {
        if (strcasecmp($productXML->sku, $sku) == 0){
            $prodObj = mapXmlToProduct($productXML);
            break;
        }
    }
    return $prodObj;
}

function mapXmlToProduct($productXML){
    $prodObj = new productData();
    $attributes = $productXML->attributes();
    $isActive = (string)$attributes['isActive'];
    $isWebAvailable = (string)$attributes['isWebAvailable'];
    $prodObj->setIsActive(parseBoolean($isActive));
    $prodObj->setIsWebAvailable(parseBoolean($isWebAvailable));
    $prodObj->setCategory($productXML->category);
    $prodObj->setSku($productXML->sku);
    $prodObj->setName($productXML->name);
    $prodObj->setDescription($productXML->description);
    $prodObj->setImage($productXML->mainimage);

    $alternateImagesXML = $productXML->alternateimages;
    foreach($alternateImagesXML->alternateimage as $alternateImage){
        $image = trim($alternateImage);
        if (strlen($image) > 0){
            $prodObj->alternateImages[] = trim($alternateImage);
        }
    }

    $prodObj->basePrice = floatval ($productXML->basePrice);

    $optionsXML = $productXML->options;
    foreach($optionsXML->option as $optionXML){
        $prodOption = new productOption();
        $prodOption->name = $optionXML->description;
        $prodOption->price = floatval ($optionXML->optionPrice);
        $prodObj->options[] = $prodOption;
    }

    $relatedProductsXML = $productXML->relatedproducts;
    foreach($relatedProductsXML->relatedsku as $sku){
        if (strlen(trim($sku)) > 0){
            $prodObj->relatedSkus[] = trim($sku);
        }
    }

    $linksXML = $productXML->links;
    foreach($linksXML->link as $linkXML){
        $prodLink = new productLink();
        $prodLink->name = $linkXML->name;
        $prodLink->url = $linkXML->url;
        $prodObj->links[] = $prodLink;
    }
    return $prodObj;
}

function parseBoolean($value){
    if (strcasecmp($value, "true") == 0){
        return true;
    }
    return false;
}

function generateProductMenu($path, $productCategory){
    $products = readDatabase($productCategory);
    $prevCat = "";
    $started = false;
    echo "<h3>Products</h3>";
    if ($path != null || strlen($path) > 0){
        $str = $path . "index.php";
        echo"<h4><a href='$str'>BACK</a></h4>";
    } else {

    }
    foreach($products as $prod) {
        if ($prod->isActive){
            $cat = $prod->category;
            if (strcasecmp($cat,$prevCat) != 0){
                if ($started){
                    echo "</ul>";
                    echo "</div>";
                    echo "</div> <!-- .Section -->";
                    $started = false;
                }
                echo "<div class='section'>";
                echo "<h4>$prod->category</h4>";
                echo "<div class='inner'>";
                echo "<ul>";
                $started = true;
                $prevCat = $prod->category;
            }
            $str = $path .  "product.php?SKU=$prod->sku";
            echo"<li><a href='$str'>$prod->name</a></li>";
        }
    }
    if ($started){
        echo "</ul>";
        echo "</div>";
        echo "</div>";
        $started = false;
    }
}
?>