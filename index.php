<?php
include_once("NoMo/nomo_SDK_lib.php");
include_once("productDataParser.php");

session_start();

    $Checkout = NOMO_SESSION_IS_CHECKOUT_IN_PROGRESS();
    $LastSku = $_SESSION[LastProductSku];
session_write_close();

    if (empty($LastSku))
        $LastSku = "TBS100101";

    header("Location:product.php?SKU=".$LastSku);

?>
