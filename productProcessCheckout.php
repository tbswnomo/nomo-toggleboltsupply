<?php
ini_set('display_errors', 'On');
include_once("NoMo/nomo_SDK_lib.php");
include_once("productDataParser.php");

session_start();
$errorHtml = "";
$inCheckout = NOMO_SESSION_IS_CHECKOUT_IN_PROGRESS();
if (!$inCheckout){
    header("Location:index.php");
    exit;
}

//If no products defined for our store or our local cart is empty, then navigate back to the cart page.
$products = $_SESSION['ShoppingCart'];
if (!isset($products) || count($products)< 1) {
    header("Location:index.php");
    exit;
}

$nomoServer = "";
if(isset($_SESSION[NOMO_FIELD_NOMO_ISSUER])) {
    $nomoServer = $_SESSION[NOMO_FIELD_NOMO_ISSUER];
}

$nomoTransactionId = "";
if (isset($_SESSION[NOMO_FIELD_TRANSACTION_ID])){
    $nomoTransactionId = $_SESSION[NOMO_FIELD_TRANSACTION_ID];
}

$nomoDeviceId = "";
if (isset($_SESSION[NOMO_FIELD_DEVICE_ID])){
    $nomoDeviceId = $_SESSION[NOMO_FIELD_DEVICE_ID];
}

$nomoUser = "";
if (isset($_SESSION[NOMO_FIELD_DEVICE_ALIAS])){
    $nomoUser = $_SESSION[NOMO_FIELD_DEVICE_ALIAS];
}

if (empty($nomoDeviceId)){
    $nomoDeviceId = NOMO_COOKIE_FETCH_DEVICE_ID();
}

$nomoContact = null;
$nomoPayment = null;
$nomoShipping = null;

//NoMo Integration Begin

//Prepare to call the NoMo SDK API for fetching the Payment terms from the user.
//The API call requires the Cart Items in a NoMo object, so first thunk our local cart to a NoMo Cart object;
//Note the NoMo Cart treats each data element as a String.
$NoMoCart = new NomoCart();

//Our local cart for this demo is a key value pair array with the key being the SKU and the Value being the Quantity so
//We will loop thur the selected products and add each to the NoMo Cart that will be sent to NoMo.
$subTotal = 0.0;
foreach ($products as $sku => $quantity){
    $item = readProduct($sku);
    //Skip products that are in the cart but not in the product database.
    if (!isset($item) || empty($item->sku))
        continue;

    $item = readProduct($sku);
    $cost = floatval($item->basePrice);
    $subTotal += $quantity * $cost;

    $NoMoItem = New NomoCartItem();
    $NoMoItem->setMerchantsku($item->getSku());
    $NoMoItem->setShortdescription($item->getName());
    $NoMoItem->setLongdescription($item->getDescription());
    $NoMoItem->setQnt(strval($quantity));
    $NoMoItem->setCost(strval($item->getBasePrice()));
    //In this demo we do not have the manufacture information so just set them to blank.
    $NoMoItem->setManufacture("");
    $NoMoItem->setManufacturesku("");
    $NoMoCart->addCartItem($NoMoItem);
}
//Calculate any additional fees that we charge in this demo
$fees = ($subTotal * 0.03);
$tax = ($subTotal * 0.0625);
$total = $subTotal + $fees + $tax;

//Load the NoMo cart with our fees and the total
$NoMoCart->setFees(strval($fees));
$NoMoCart->setFeesdesc("Packaging");
$NoMoCart->setTax(strval($tax));
$NoMoCart->setSubtotal(strval($subTotal));
$NoMoCart->setTotal(strval($total));

//Call the NoMo SDK API to request a list of possible shipping destinations the user has on file.
//Use this to establish a array of NomoShippingOptions objects that list all shipping options offered by
//the merchant for each local returned by NoMo.  This Array will be passed to the NoMo SDK Checkout API.
//The user will be asked to choose a shipping option that applies to the shipping local they have chosen.
//The NoMo SDK will return the shipping option chosen as part of the NomoShipping object.

$shippingOptions = array();
$nomo = new NOMO_SDK();
$response = $nomo->NOMO_FETCH_SHIPPING_LOCALS(
    $nomoServer,
    $nomoTransactionId,
    $nomoDeviceId
);
//Check the return status
switch($nomo->status){
    case NOMO_STATUS_SUCCESS:  // Shipping locals have been returned.
        $locals = $nomo->NOMO_GET_SHIPPING_LOCATIONS($response);
        foreach($locals as $local){
            /* @var $local NomoShippingOption */

            //Determine all merchant shipping options that apply to this local.
            //Add each to the array that is to be passed to checkout.
            $options = calculateShippingOptions($local, $total);
            $shippingOptions = array_merge($shippingOptions, $options);
        }

        break;
    default:
        // An error was encountered during the NoMo Shipping Locals fetch.
        // For this demo we just echo the result to the page along with the response.
        $errorHtml .=  "<br>Fetch Locals Error: ". $response;
        $errorHtml .= "<br>NoMo Error: (".$nomo->status.") - ".NomoStatusDescriptions::getDescription($nomo->status);
        $errorHtml .= "<br>NomoServer: ". $nomo->issuer;
        $errorHtml .= "<br>NomoDevice: ". $nomo->deviceId;
        $errorHtml .= "<br>NomoTransaction ID: ".$nomo->transactionId;
        $errorHtml .= "<br>NomoCart: ". $cartHexDump;
        break;
}

function calculateShippingOptions(NomoShippingOption $location, $total){
    $result = Array();

    $opt = new NomoShippingOption();
    $opt->setScountry($location->getScountry());
    $opt->setSpostal($location->getSpostal());
    $opt->setSoption("UPS - Ground ".$location->getSpostal());
    $opt->setScost(strval($total*.02));
    array_push($result, $opt);

    $opt = new NomoShippingOption();
    $opt->setScountry($location->getScountry());
    $opt->setSpostal($location->getSpostal());
    $opt->setSoption("UPS - Overnight ".$location->getSpostal());
    $opt->setScost(strval($total*.05));
    array_push($result, $opt);

    $opt = new NomoShippingOption();
    $opt->setScountry($location->getScountry());
    $opt->setSpostal($location->getSpostal());
    $opt->setSoption("FedEx - Standard ".$location->getSpostal());
    $opt->setScost(strval($total*.03));
    array_push($result, $opt);

    $opt = new NomoShippingOption();
    $opt->setScountry($location->getScountry());
    $opt->setSpostal($location->getSpostal());
    $opt->setSoption("FedEx - Overnight ".$location->getSpostal());
    $opt->setScost(strval($total*.04));
    array_push($result, $opt);

    return $result;
}


//Call the NoMo SDK API to request payment information for this transaction.

$checkoutResponse = $nomo->NOMO_CHECKOUT($nomoServer,
    $nomoTransactionId,
    $nomoDeviceId,
    $NoMoCart,
    $shippingOptions,
    array(
        NOMO_FIELD_VALUE_CHECKOUT_REQUIRED_PHONE,
        NOMO_FIELD_VALUE_CHECKOUT_REQUIRED_EMAIL,
        NOMO_FIELD_VALUE_CHECKOUT_REQUIRED_NAME
    )
);

//Check the return status for success, an indication that the user responded with
//either payment terms or has declined the transaction. If not success
switch($nomo->status){
    case NOMO_STATUS_SUCCESS:  // Payment data returned by user
        // The Json response from the NOMO_CHECKOUT SDK call will contain the Payment data, Shipping data and Contact Data
        // the user has choosen for this transaction in a JSON form.
        // You may work with the Json response as is or use the supplied NoMo helper methods and objects. We recommend
        // using the helper method and objects so that in future releases you will not have to adjust your parsing code.
        // In this demo we will use the NoMo Payment, Shipping, and Contact objects.
        $nomoContact = $nomo->NOMO_GET_CONTACT($checkoutResponse);
        $nomoPayment = $nomo->NOMO_GET_PAYMENT($checkoutResponse);
        $nomoShipping = $nomo->NOMO_GET_SHIPPING($checkoutResponse);

        //Make the necessary call to use the payment information to make the desired payment.

        //Let NoMo know that this transaction has complete the checkout.
        //supply the up to date cart information.
        $completeResponse = $nomo->NOMO_CHECKOUT_COMPLETE(NOMO_FIELD_TRANSACTION_STATE_COMPLETE, $NoMoCart);
        NOMO_SESSION_CLEAR(true);
        break;
    case NOMO_STATUS_POLL_DECLINED:  // User declined the checkout.
        //The User rejected the transaction.

        //For this demo we will return to the home page. Potentially you may take the user to another
        //page to find out why, or entice with some offer to complete the transaction.
        NOMO_SESSION_CLEAR(true);
        header("Location:index.php");
        exit;
        break;
    case NOMO_STATUS_POLL_DATA_NOT_AVAILABLE:
        // This return indicates the user failed to authenticate the payment request in time.
        // This usually indicates the user has taken too long or there is a internet slow down
        // causing lags or dropped messages. If this happens frequently contact NoMo for advice
        // on changing the timing settings in the NoMo SDK.

        // To handle this event for this transaction you could retry the checkout, cancel the
        //transaction and allow the user to leave. Let the user know the internet is performing
        //slowly and give them another chance at checkout.

        // For this demo we will simply cancel the transaction and navigate back to the Cart Page
        // so the user may try again.
        NOMO_SESSION_CLEAR(false);
        header("Location:productCart.php?".NOMO_FIELD_STATUS."=".$nomo->status);
        exit;
    case NOMO_STATUS_TRANSACTION_INVALID_STATE:
        //Transaction is not is a valid state.

        //When this happens cancel the transaction and
        //Restart the checkout process. If the domain has signed up for Fast Checkout you can restart the
        //transaction as a fast checkout transaction without bothering the user.

        //For this demo, we will cancel the current transaction take the user back to the cart page to have them restart the checkout.
        NOMO_SESSION_CLEAR(false);
        header("Location:productCart.php?".NOMO_FIELD_STATUS."=".$nomo->status);
        exit;
    case NOMO_STATUS_TRANSACTION_EXPIRED:
        //Transaction has expired before it could be authenticated.

        //When this happens you can either cancel the transaction or
        //Restart the checkout process. If the domain has signed up for Fast Checkout you can restart the
        //transaction as a fast checkout transaction without bothering the user.

        //For this demo, we will take the user back to the cart page to have them restart the checkout.
        NOMO_SESSION_CLEAR(false);
        header("Location:productCart.php?".NOMO_FIELD_STATUS."=".$nomo->status);
        exit;
    default:
        // An error was encountered during the NoMo checkout process.
        // You should contact NoMo support with the following data:
        //      NoMo Server sending checkout to.
        //      NoMo Transaction Id
        //      NoMo DeviceId
        //      NoMo Cart in a hex dump.
        //             To take a dump of the cart use the cart and SDK helper methods to get the
        //             cart as a json string and then the hex dump of that string.
        //             Ex:  $cartJsonString = $NoMoCart->toNoMoJson(true)
        //                  $cartHexDump = $nomo->etStringAsHex($cartJsonString, true)
        $cartJsonString = $NoMoCart->toNoMoJson(true);
        $cartHexDump = $nomo->getStringAsHex($cartJsonString,true);
        // For this demo we just echo the result to the page along with the response.
        $errorHtml =  "<br>Checkout Response: ". $checkoutResponse;
        $errorHtml .= "<br>NoMo Error: (".$nomo->status.") - ".NomoStatusDescriptions::getDescription($nomo->status);
        $errorHtml .= "<br>NomoServer: ". $nomo->issuer;
        $errorHtml .= "<br>NomoDevice: ". $nomo->deviceId;
        $errorHtml .= "<br>NomoTransaction ID: ".$nomo->transactionId;
        $errorHtml .= "<br>NomoCart: ". $cartHexDump;
        break;
}

// NoMo Integration End


?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <?php include("head.php"); ?>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser.
                <a href="http://browsehappy.com/">Upgrade your browser today</a> or
                <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a>
                to better experience this site.</p>
        <![endif]-->

        <div id="wrapper">
            <header>
                <?php include("header.php"); ?>
            </header>

            <section>
                <div id="container">
                    <div id="content" style="margin-right: 20px;">
                        <?php
                        if (!empty($errorHtml)){
                            echo '<h2>NoMo SDK returned Error:</h2>';
                            echo "<article>";
                            echo ($errorHtml);
                            echo "</article>";
                        }else{
                        ?>
                            <article>
                                <!-- this page displays the checkout confirmation page.-->
                                <h2>Thank you <?php if (!empty($nomoUser)) echo ($nomoUser." ")?>for shopping at Togglebolt Supply</h2>

                                <table>
                                    <tr>
                                        <td colspan="4" >
                                            <h3>Payment Information</h3>
                                            <table>
                                                <?php echo getPaymentDisplay($nomoPayment);?>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp</td></tr>
                                    <tr>
                                        <td style=" vertical-align: top;">
                                            <h3>Billing Address</h3>
                                            <table>
                                            <?php echo getBillingAddressDisplay($nomoPayment); ?>
                                            </table>
                                        </td>
                                        <td>&nbsp</td>
                                        <td>&nbsp</td>
                                        <td style=" vertical-align: top;">
                                            <h3>Shipping Address</h3>
                                            <table>
                                            <?php echo getShippingDisplay($nomoShipping); ?>
                                            </table>
                                        </td>
                                        <td>&nbsp</td>
                                        <td>&nbsp</td>
                                        <td style=" vertical-align: top;">
                                            <h3>Contact Information</h3>
                                            <table>
                                            <?php echo getContactDisplay($nomoContact)?>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <h3>Your Order:</h3>
                                <?php echo getCartDisplay($products); ?>
                            </article>
                        <?php
                        }
                        ?>
                    </div><!-- #content-->
                </div><!-- #container-->

                <?php include("menuSide.php"); ?>
            </section>

            <footer>
                <?php include("copyright.php"); ?>
            </footer>

        </div><!-- #wrapper -->

    </body>
</html>

<?php

function getContactDisplay($nomoContact){
    $output = "";
    if (!isset($nomoContact)){
        return $output;
    }

    if (!is_object($nomoContact) || !($nomoContact instanceof NomoContact))
        return $output;

    if (!$nomoContact->isValid())
        return $output;

    // Build the html table we will output.
    $output .= addField("First Name", $nomoContact->getFname());
    $output .= addField("Last Name", $nomoContact->getLname());
    $output .= addField("Phone", $nomoContact->getPhone());
    $output .= addField("Email", $nomoContact->getEmail());
    return $output;
}

function getShippingDisplay($nomoShipping){
    /* @var  $nomoShipping NomoShipping */

    $output = "";
    if (!isset($nomoShipping)){
        return $output;
    }

    if (!is_object($nomoShipping) || !($nomoShipping instanceof NomoShipping))
        return $output;

    if (!$nomoShipping->isValid())
        return $output;

    // Build the html table we will output.
    $output .= addField("Attention:"       ,$nomoShipping->getAttention());
    $output .= addField("Organization:"    ,$nomoShipping->getOrganization());
    $output .= addField("Residential:"     ,$nomoShipping->getResidential());
    $output .= addField("Street1:"         ,$nomoShipping->getStreet1());
    $output .= addField("Street2:"         ,$nomoShipping->getStreet2());
    $output .= addField("City:"            ,$nomoShipping->getCity());
    $output .= addField("State:"           ,$nomoShipping->getState());
    $output .= addField("Postal:"          ,$nomoShipping->getPostalcode());
    $output .= addField("Carrier:"         ,$nomoShipping->getShipoption());

    return $output;
}

function cc_masking($number) {
    return substr($number, 0, 4) . str_repeat("X", strlen($number) - 8) . substr($number, -4);
}

function getPaymentDisplay($nomoPayment){
    /* @var  $nomoPayment NomoPayment */

    $output = "";
    if (!isset($nomoPayment)){
        return $output;
    }

    if (!is_object($nomoPayment) || !($nomoPayment instanceof NomoPayment))
        return $output;

    if (!$nomoPayment->isValid())
        return $output;

    $output .= addField("Payment Network:"       ,$nomoPayment->getNetwork());

    $ccDisplay = cc_masking($nomoPayment->getNumber());

    if ($nomoPayment->getNetwork() == NOMO_FIELD_VALUE_PAYMENT_NETWORK_DEBIT ||
        $nomoPayment->getNetwork() == NOMO_FIELD_VALUE_PAYMENT_NETWORK_VISA ||
        $nomoPayment->getNetwork() == NOMO_FIELD_VALUE_PAYMENT_NETWORK_MASTER_CARD){

        $output .= addField("Name On Card:"     ,$nomoPayment->getNameoncard());
        $output .= addField("Card Number:"      ,$ccDisplay);
        $output .= addField("Expiration Date:"  ,$nomoPayment->getExpiration());
        $output .= addField("CVC:"              ,$nomoPayment->getCvc());
    }else if($nomoPayment->getNetwork() == NOMO_FIELD_VALUE_PAYMENT_NETWORK_PAYPAL){
        $output .= addField("PayPal User Id:"   ,$nomoPayment->getUsername());
        $output .= addField("Password:"         ,$nomoPayment->getPassword());
    }
    return $output;
}

function getBillingAddressDisplay($nomoPayment){
    /* @var  $nomoPayment NomoPayment */

    $output = "";
    if (!isset($nomoPayment)){
        return $output;
    }

    if (!is_object($nomoPayment) || !($nomoPayment instanceof NomoPayment))
        return $output;

    if (!$nomoPayment->isValid())
        return $output;

    $output .= addField("Street1:"         ,$nomoPayment->getStreet1());
    $output .= addField("Street2:"         ,$nomoPayment->getStreet2());
    $output .= addField("City:"            ,$nomoPayment->getCity());
    $output .= addField("State:"           ,$nomoPayment->getState());
    $output .= addField("Postal:"          ,$nomoPayment->getPostalcode());
    return $output;
}

function addField($prompt, $value){
    $output = "";
    $data = trim(strval($value));
    if (!empty($data))
        $output .= "<tr><td>$prompt</td><td>$data</td></tr>"."\n";
    return $output;
}

function getCartDisplay($products){
    $output = "";
    $output .='<table>'."\n";
    $output .='    <tr>';
    $output .='        <th align="left">Description</th>';
    $output .='        <th>&nbsp;</th>';
    $output .='        <th align="right">Price</th>';
    $output .='        <th>&nbsp;</th>';
    $output .='        <th align="right">Quantity</th>';
    $output .='        <th>&nbsp;</th>';
    $output .='        <th align="right">Cost</th>';
    $output .='    </tr>'."\n";

    $subTotal = 0.0;
    foreach ($products as $sku => $quantity){
        $item = readProduct($sku);
        $cost = floatval($item->basePrice);
        $itemPrice = "$" . number_format($cost, 2, ".", ",");
        $itemQnt = $quantity;
        $itemDesc = $item->name;
        $itemCost = "$" . number_format(($cost * $quantity), 2, ".", ",");
        $subTotal += $quantity * $cost;

        $output .= '    <tr>';
        $output .= '<td align="left">'.$itemDesc.'</td>';
        $output .= '<td>&nbsp;</td>';
        $output .= '<td align="right">'.$itemPrice.'</td>';
        $output .= '<td>&nbsp;</td>';
        $output .= '<td align="right">'.$itemQnt.'</td>';
        $output .= '<td>&nbsp;</td>';
        $output .= '<td  align="right">'.$itemCost.'</td>';
        $output .= '<td>&nbsp;</td>';
        $output .= '</tr>'."\n";
    }
    $output .= '</table>'."\n";

    $fees = ($subTotal * 0.03);
    $tax = ($subTotal * 0.0625);
    $shipping = ($subTotal * 0.04);
    $total = $subTotal + $fees + $shipping + $tax;
    $displaySubTotal = "$" . number_format($subTotal, 2, ".", ",");
    $displayTax = "$" . number_format($tax, 2, ".", ",");
    $displayFees = "$" . number_format($fees, 2, ".", ",");
    $displayShipping = "$" . number_format($shipping, 2, ".", ",");
    $displayTotal = "$" . number_format($total, 2, ".", ",");

    $output .= '<table>'."\n";
    $output .= '    <tr><td>&nbsp;</td></tr>'."\n";
    $output .= '    <tr><td>Sub Total</td><td>&nbsp;</td><td align="right">'.$displaySubTotal.'</td></tr>'."\n";
    $output .= '    <tr><td>Packaging and processing Fees</td><td>&nbsp;</td><td align="right">'.$displayFees.'</td></tr>'."\n";
    $output .= '    <tr><td>Tax</td><td>&nbsp;</td><td align="right">'.$displayTax.'</td></tr>'."\n";
    $output .= '    <tr><td>Shipping</td><td>&nbsp;</td><td align="right">'.$displayShipping.'</td></tr>'."\n";
    $output .= '    <tr><td><b>Total<b></td><td>&nbsp;</td><td align="right"><b>'.$displayTotal.'</b></td></tr>'."\n";
    $output .= '</table>'."\n";

    return $output;
}

?>
