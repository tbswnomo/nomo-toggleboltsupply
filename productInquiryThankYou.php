<?php
include_once("productDataParser.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <?php include("head.php"); ?>
</head>

<body>

<div id="wrapper">
	<header>
		<div id="description">Hand built quality products.</div>
        <?php include("menuTop.php"); ?>
    </header>

	<section>
        <div id="container">
            <div id="content">
                <article>
                    <h2><a href="#" rel="bookmark">Product Inquiry</a></h2>
                    <div class="entry">
                        <p>
                            Thank you for your interest in our products. We will get back to you with in 48 hours.
                        </p>
                    </div>
                </article>
            </div><!-- #content-->
		</div><!-- #container-->
	</section>

    <?php include("menuSide.php"); ?>

    <footer>
        <?php include("copyright.php"); ?>
    </footer>
</div><!-- #wrapper -->
</body>
</html>
