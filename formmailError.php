<?php
include_once("productDataParser.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
	<meta charset="UTF-8">
	<title>Dragon Fabrication</title>
	<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="print.css" type="text/css" media="print" />
	<!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><!
    [endif]-->
</head>

<body>

<div id="wrapper">
	<header>
		<div id="description">Toggle Bolts for every need.</div>
        <?php include("menuTop.php"); ?>
    </header>

	<section>
        <div id="container">
            <div id="content">
                <article>
                    <h2><a href="#" rel="bookmark">Contact</a></h2>
                    <div class="entry">
                        <h4>We apologies for the inconvenience.</h4>
                        <p>
                            A problem with our mail service has caused your inquiry to not be sent.
                            <br>
                            Please send your inquiry to <b>sales@toggleboltsw.com</b> by another email program.
                        </p>
                    </div>
                </article>
            </div><!-- #content-->
		</div><!-- #container-->

        <?php include("menuSide.php"); ?>

        <footer>
            <?php include("copyright.php"); ?>
	</footer>

</div><!-- #wrapper -->
</body>
</html>
